
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserModel } from './login/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private USER_SESSION = 'user_key';
    private credentials: BehaviorSubject<User>;
    private isLoggedIn: boolean;
    private jwt: JWT;
    private personaExists = true;

    constructor(protected http: HttpClient, private router: Router,
        @Inject(SESSION_STORAGE) private storage: StorageService
    ) {
        this.credentials = new BehaviorSubject<User>(null);
        this.getUser().subscribe(user => {
            if (user != undefined && user.isLoggedIn != undefined) {
                this.isLoggedIn = user.isLoggedIn;

                try {
                    this.jwt = jwtDecode(user.accessToken) as JWT;
                  } catch (Error) {
                    console.log(Error);
                  }
            } else {
                this.isLoggedIn = false;
            }
        });

        const userSession = this.storage.get(this.USER_SESSION);
        if (userSession != undefined && userSession.isLoggedIn != undefined) {
            this.isLoggedIn = userSession.isLoggedIn;
            this.credentials.next(userSession);
        } else {
            this.isLoggedIn = false;
        }
    }

    public getUser(): Observable<User> {
        return this.credentials.asObservable();
    }

    public getAuthorities() {
        return this.jwt.authorities;
    }

    public getSub() {
        return this.jwt.sub;
    }

    public getIsLoggedIn(): boolean {
        return this.isLoggedIn;
    }

    public login(username: string, password: string): void {
        this.http.post<HttpResponse<any>>(`/api/auth`, { username: username, password: password }, { observe: 'response' })
        .subscribe((resp) => {
            const user = {} as User;
            user.isLoggedIn = true;
            user.accessToken = resp.headers.get('authorization');
            this.credentials.next(user);
            this.storage.set(this.USER_SESSION, user);
            if(this.personaExists) {
                this.goToHome();
            }
        });
    }

    public goToHome() {
        this.personaExists = true;
        this.router.navigateByUrl('/aluno');
    }

    public createUser(user: UserModel): Observable<UserModel> {
        return this.http.post(`/api/users`, user).pipe(map(value => value as UserModel));
    }

    public logout() {
        const user = {} as User;
        this.storage.set(this.USER_SESSION, user);
        this.credentials.next(user);
        this.router.navigateByUrl('/login');
    }

}

interface JWT {
    authorities: string[];
    exp: number;
    iat: number;
    sub: string;
}
