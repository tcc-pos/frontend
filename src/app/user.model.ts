interface User {
    isLoggedIn: boolean;
    accessToken: string;
}