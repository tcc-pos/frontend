import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { UserService } from 'src/app/user.service';
import { UserModel } from '../../models/user.model';

@Component({
    selector: 'cadastro-modal',
    templateUrl: './cadastro-modal.component.html',
    styleUrls: ['./cadastro-modal.component.css']
})
export class CadastroModalComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<CadastroModalComponent>,
        protected userService: UserService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            documento: new FormControl(null, [Validators.required])
            , password: new FormControl(null, [Validators.required])
        });

        dialogRef.disableClose = true;
    }

    private getDataFromForm() {
        const user = {} as UserModel;
        user.username = this.form.get('documento').value;
        user.password = this.form.get('password').value;

        return user;
    }

    protected onOkClick(): void {
        const user = this.getDataFromForm();
        this.userService.createUser(user).subscribe(
            (success) => {
                const subscription = this.userService.getUser().subscribe((resp: User) => {
                    if (resp != undefined && resp.accessToken != undefined) {
                        /*const persona = {} as PersonaModel;
                        persona.documento = user.documento;
                        persona.nome = user.nome;
                        this.userService.createPersona(persona).subscribe((resp) => {
                            subscription.unsubscribe();
                            this.userService.goToHome();
                            this.dialogRef.close(success);
                        })*/
                        subscription.unsubscribe();
                        this.userService.goToHome();
                        this.dialogRef.close(success);
                    }
                });
                this.userService.login(user.username as unknown as string, user.password);
            },
            (fail) => {
                console.log(fail);
                this.dialogRef.close();
            }
        );
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}