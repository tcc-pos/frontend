import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { MatDialog } from '@angular/material';
import { CadastroModalComponent } from './modais/cadastro/cadastro-modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public message: string;
  private subscription: Subscription = new Subscription();
  private form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    protected router: Router,
    protected userService: UserService,
    protected customDialog: MatDialog
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  cadastrar() {
    const dialogRef = this.customDialog.open(
      CadastroModalComponent
    );
  }

  login() {
    this.userService.login(this.form.controls['username'].value, this.form.controls['password'].value);
  }

  OnDestroy(): void {
    if (this.subscription && this.subscription.closed === false) {
      this.subscription.unsubscribe();
    }
  }

}
