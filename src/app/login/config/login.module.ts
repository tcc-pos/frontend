import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatMenuModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatGridListModule,
    MatChipsModule,
    MatBadgeModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule
} from '@angular/material';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from '../login.component';
import { NgxMaskModule } from 'ngx-mask';
import { CadastroModalComponent } from '../modais/cadastro/cadastro-modal.component';

@NgModule({
    declarations: [
        LoginComponent,
        CadastroModalComponent
    ],
    imports: [
        CommonModule,
        MatTableModule,
        MatBadgeModule,
        ReactiveFormsModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCheckboxModule,
        MatSortModule,
        MatPaginatorModule,
        MatMenuModule,
        MatCardModule,
        MatListModule,
        MatGridListModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        LoginRoutingModule,
        MatOptionModule,
        MatSelectModule,
        MatDialogModule,
        FormsModule,
        NgxMaskModule.forRoot()
    ],
    exports: [
        LoginComponent
    ],
    entryComponents: [
        CadastroModalComponent
    ]
})
export class LoginModule { }
