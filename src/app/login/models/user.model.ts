export interface UserModel {
    username: string,
    password: string,
    enabled: boolean
}