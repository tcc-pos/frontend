import { tap, catchError, finalize } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, Subscription, throwError } from 'rxjs';
import { UserService } from './user.service';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {

  private subscription: Subscription = new Subscription();
  private user$: Observable<User>;
  private accessToken: string;

  constructor(
    protected userService: UserService,
  ) {
    this.user$ = userService.getUser();
    this.subscription.add(this.user$.subscribe(user => {
      if(user != undefined && user.accessToken != undefined) {
        this.accessToken = user.accessToken;
      }
    }));
  }

  private setToken(req: HttpRequest<any>): HttpRequest<any> {
    let modifiedReq: HttpRequest<any>;
    if (this.accessToken !== undefined) {
      modifiedReq = req.clone({
        headers: req.headers.set('Authorization', this.accessToken),
      });
    } else {
      modifiedReq = req;
    }

    return modifiedReq;
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.setToken(req)).pipe(
      tap((resp: HttpResponse<any>) => {

      })
      , catchError(
        (resp: HttpErrorResponse) => {
          this.onError(resp);

          return throwError(resp);
        }
      )
      , finalize(() => {
      })
    );
  }

  private onError(resp: HttpErrorResponse): void {
    if(resp.error != undefined && resp.error.length > 0){
      alert(resp.error);
    } else {
      alert(resp.statusText);
    }
  }

  OnDestroy(): void {
    if (this.subscription && this.subscription.closed === false) {
      this.subscription.unsubscribe();
    }
  }
}
