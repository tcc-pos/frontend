import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-not-found',
    template: `
      <h2>
        Página não encontrada
      </h2>
      <p>Talvez queira ir para a página <a routerLink="">principal</a></p>
    `
})
export class NotFoundComponent implements OnInit {
    constructor() { }

    ngOnInit() {

    }
}