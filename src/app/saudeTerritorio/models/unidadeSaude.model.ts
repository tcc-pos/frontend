interface UnidadeSaudeModel {
    id: number;
    cnes: string;
    cnpj: string;
    tipoEstabelecimento: string;
    nomeFantasia: string;
    endereco: string;
    cep: string;
    telefone: string;
    email: string;
}