import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaudeTerritorioComponent } from './saudeTerritorio.component';

describe('SaudeTerritorioComponent', () => {
  let component: SaudeTerritorioComponent;
  let fixture: ComponentFixture<SaudeTerritorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaudeTerritorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaudeTerritorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
