import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable, BehaviorSubject } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { SaudeTerritorioModalEditComponent } from './modais/edit/saudeTerritorio-modal-edit.component';
import { SaudeTerritorioDeleteComponent } from './modais/delete/saudeTerritorio-modal-delete.component';
import { SaudeTerritorioService } from './services/saudeTerritorio.service';
import { SaudeTerritorioAddComponent } from './modais/add/saudeTerritorio-modal-add.component';

@Component({
  selector: 'app-saude-territorio',
  templateUrl: './saudeTerritorio.component.html',
  styleUrls: ['./saudeTerritorio.component.css']
})
export class SaudeTerritorioComponent implements OnInit {

  protected dataSource: Observable<UnidadeSaudeModel[]>;
  protected behaviorSubject: BehaviorSubject<UnidadeSaudeModel[]> = new BehaviorSubject<UnidadeSaudeModel[]>(null);
  protected displayedColumns: string[] = ['id', 'cnes','cnpj','tipoEstabelecimento','nomeFantasia', 'endereco', 'cep','telefone','email', 'actions'];

  constructor(protected consultaMedicaService: SaudeTerritorioService, protected customDialog: MatDialog) {
    this.dataSource = this.behaviorSubject.asObservable();
  }

  ngOnInit() {
    this.consultaMedicaService.searchSaudeTerritorio().subscribe((data: UnidadeSaudeModel[]) => {
      if (!isNullOrUndefined(data)) {
        this.behaviorSubject.next(data);
      } else {
        this.behaviorSubject.next([]);
      }
    });
  }

  public edit(paciente: PacienteModel) {
    const dialogRef = this.customDialog.open(
      SaudeTerritorioModalEditComponent
      , { data: paciente }
    );

    dialogRef.afterClosed().subscribe((result: UnidadeSaudeModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.getValue().forEach(i => {
          if (i.id === result.id) {
            Object.assign(i, result);
          }
        });
      }
    });
  }

  public add() {
    const dialogRef = this.customDialog.open(
      SaudeTerritorioAddComponent
    );

    dialogRef.afterClosed().subscribe((result: UnidadeSaudeModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.getValue().push(result);
        this.behaviorSubject.next(this.behaviorSubject.getValue());
      }
    });
  }

  public delete(UnidadeSaudeModel: UnidadeSaudeModel) {
    const dialogRef = this.customDialog.open(
      SaudeTerritorioDeleteComponent
      , { data: UnidadeSaudeModel }
    );

    dialogRef.afterClosed().subscribe((result: UnidadeSaudeModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.next(this.behaviorSubject.getValue().filter((UnidadeSaudeModel: UnidadeSaudeModel) => {
          return UnidadeSaudeModel.id != result.id;
        }));
      }
    });
  }

}
