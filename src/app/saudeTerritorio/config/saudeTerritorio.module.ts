import { NgModule } from '@angular/core';

import { SaudeTerritorioRoutingModule } from './saudeTerritorio-routing.module';
import { SaudeTerritorioComponent } from '../saudeTerritorio.component';
import { MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatTableModule, MatTabsModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { CommonModule } from '@angular/common';
import { SaudeTerritorioDeleteComponent } from '../modais/delete/saudeTerritorio-modal-delete.component';
import { SaudeTerritorioModalEditComponent } from '../modais/edit/saudeTerritorio-modal-edit.component';
import { SaudeTerritorioAddComponent } from '../modais/add/saudeTerritorio-modal-add.component';

@NgModule({
    declarations: [
        SaudeTerritorioComponent,
        SaudeTerritorioDeleteComponent,
        SaudeTerritorioModalEditComponent,
        SaudeTerritorioAddComponent
    ],
    imports: [
        SaudeTerritorioRoutingModule,
        MatTableModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatTabsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        CommonModule,
        NgxMaskModule.forRoot()
    ],
    exports: [
        SaudeTerritorioComponent
    ],
    entryComponents: [
        SaudeTerritorioDeleteComponent,
        SaudeTerritorioModalEditComponent,
        SaudeTerritorioAddComponent
    ]
})
export class SaudeTerritorioModule { }