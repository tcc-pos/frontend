import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaudeTerritorioComponent } from '../saudeTerritorio.component';

const routes: Routes = [
    {
        path: '',
        component: SaudeTerritorioComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SaudeTerritorioRoutingModule { }