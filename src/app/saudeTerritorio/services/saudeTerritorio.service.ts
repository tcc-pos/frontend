
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SaudeTerritorioService {

  constructor(protected http: HttpClient) { }

  public searchSaudeTerritorio(): Observable<UnidadeSaudeModel[]> {
    return this.http.get(`/api/consultas-medicas/unidades-saude/`).pipe(map(value => value as UnidadeSaudeModel[]));
  }

  public searchSaudeTerritorioById(id: number): Observable<UnidadeSaudeModel> {
    return this.http.get(`/api/consultas-medicas/unidades-saude/${id}`).pipe(map(value => value as UnidadeSaudeModel));
  }

  public createSaudeTerritorio(unidadeSaudeModel: UnidadeSaudeModel): Observable<UnidadeSaudeModel> {
    return this.http.post(`/api/consultas-medicas/unidades-saude`, unidadeSaudeModel).pipe(map(value => value as UnidadeSaudeModel));
  }

  public updateSaudeTerritorio(unidadeSaudeModel: UnidadeSaudeModel): Observable<UnidadeSaudeModel> {
    return this.http.put(`/api/consultas-medicas/unidades-saude/${unidadeSaudeModel.id}`, unidadeSaudeModel).pipe(map(value => value as UnidadeSaudeModel));
  }

  public deleteSaudeTerritorio(id: number): Observable<void> {
    return this.http.delete(`/api/saude-territorio/unidades-saude/${id}`).pipe(map(value => value as unknown as void));
  }

}
