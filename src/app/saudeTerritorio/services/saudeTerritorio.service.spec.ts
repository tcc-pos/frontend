import { TestBed } from '@angular/core/testing';

import { SaudeTerritorioService } from './saudeTerritorio.service';

describe('SaudeTerritorioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaudeTerritorioService = TestBed.get(SaudeTerritorioService);
    expect(service).toBeTruthy();
  });
});
