import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SaudeTerritorioService } from '../../services/saudeTerritorio.service';
@Component({
    selector: 'saudeTerritorio-modal-delete',
    templateUrl: './saudeTerritorio-modal-delete.component.html',
    styleUrls: ['./saudeTerritorio-modal-delete.component.css']
})
export class SaudeTerritorioDeleteComponent {

    constructor(
        public dialogRef: MatDialogRef<SaudeTerritorioDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) public data: UnidadeSaudeModel, protected saudeTerritorioService: SaudeTerritorioService) {
        dialogRef.disableClose = true;
    }

    protected onOkClick(): void {
        this.saudeTerritorioService.deleteSaudeTerritorio(this.data.id).subscribe(
            (success) => {
                this.dialogRef.close(this.data);
            },
            (fail) => {
                console.log(fail);
                this.dialogRef.close();
            }
        );
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}