import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SaudeTerritorioService } from '../../services/saudeTerritorio.service';

@Component({
    selector: 'saudeTerritorio-modal-edit',
    templateUrl: './saudeTerritorio-modal-edit.component.html',
    styleUrls: ['./saudeTerritorio-modal-edit.component.css']
})
export class SaudeTerritorioModalEditComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<SaudeTerritorioModalEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: UnidadeSaudeModel, protected saudeTerritorioService: SaudeTerritorioService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            cnes: new FormControl(data.cnes, [Validators.required])
            , cnpj: new FormControl(data.cnpj, [Validators.required])
            , tipoEstabelecimento: new FormControl(data.tipoEstabelecimento, [Validators.required])
            , nomeFantasia: new FormControl(data.nomeFantasia, [Validators.required])
            , endereco: new FormControl(data.endereco, [Validators.required])
            , cep: new FormControl(data.cep, [Validators.required])
            , telefone: new FormControl(data.telefone, [Validators.required])
            , email: new FormControl(data.email, [Validators.required])
        });
        dialogRef.disableClose = true;
    }

    private getDataFromForm(): UnidadeSaudeModel {
        const UnidadeSaudeModel = {} as UnidadeSaudeModel;
        UnidadeSaudeModel.cnes = this.form.get('cnes').value;
        UnidadeSaudeModel.cnpj = this.form.get('cnpj').value;
        UnidadeSaudeModel.tipoEstabelecimento = this.form.get('tipoEstabelecimento').value;
        UnidadeSaudeModel.nomeFantasia = this.form.get('nomeFantasia').value;
        UnidadeSaudeModel.endereco = this.form.get('endereco').value;
        UnidadeSaudeModel.cep = this.form.get('cep').value;
        UnidadeSaudeModel.telefone = this.form.get('telefone').value;
        UnidadeSaudeModel.email = this.form.get('email').value;

        return UnidadeSaudeModel;
    }

    protected onOkClick(): void {
        const newUnidadeSaudeModel = this.getDataFromForm();
        newUnidadeSaudeModel.id = this.data.id;
        this.saudeTerritorioService.updateSaudeTerritorio(newUnidadeSaudeModel).subscribe(
            (success) => {
                this.dialogRef.close(success);
            },
            (fail) => {
                console.log(fail);
                this.dialogRef.close();
            }
        );
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}