import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SaudeTerritorioService } from '../../services/saudeTerritorio.service';

@Component({
    selector: 'saudeTerritorio-modal-add',
    templateUrl: './saudeTerritorio-modal-add.component.html',
    styleUrls: ['./saudeTerritorio-modal-add.component.css']
})
export class SaudeTerritorioAddComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<SaudeTerritorioAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: UnidadeSaudeModel, protected saudeTerritorioService: SaudeTerritorioService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            nome: new FormControl(null, [Validators.required])
            , endereco: new FormControl(null, [Validators.required])
            , cep: new FormControl(null, [Validators.required])
        });
        dialogRef.disableClose = true;
    }

    private getDataFromForm(): UnidadeSaudeModel {
        const UnidadeSaudeModel = {} as UnidadeSaudeModel;
        UnidadeSaudeModel.nome = this.form.get('cnes').value;
        UnidadeSaudeModel.nome = this.form.get('cnpj').value;
        UnidadeSaudeModel.nome = this.form.get('tipoEstabelecimento').value;
        UnidadeSaudeModel.nome = this.form.get('nomeFantasia').value;
        UnidadeSaudeModel.endereco = this.form.get('endereco').value;
        UnidadeSaudeModel.cep = this.form.get('cep').value;
        UnidadeSaudeModel.cep = this.form.get('telefone').value;
        UnidadeSaudeModel.cep = this.form.get('email').value;

        return UnidadeSaudeModel;
    }

    protected onOkClick(): void {
        this.saudeTerritorioService.createSaudeTerritorio(this.getDataFromForm()).subscribe(
            (success) => {
                this.dialogRef.close(success);
            },
            (fail) => {
                console.log(fail);
                this.dialogRef.close();
            }
        );
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}