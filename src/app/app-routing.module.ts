import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './authGuardService';
import { ForbiddenComponent } from './forbiddenComponent';
import { NotFoundComponent } from './notFoundComponent';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: './login/config/login.module#LoginModule',
  },
  {
    path: 'aluno',
    loadChildren: './aluno/config/aluno.module#AlunoModule',
    canActivateChild: [AuthGuardService]
  },
  {
    path: 'consulta-medica',
    loadChildren: './consultaMedica/config/consultaMedica.module#ConsultaMedicaModule',
    canActivateChild: [AuthGuardService]
  },
  {
    path: 'saude-territorio',
    loadChildren: './saudeTerritorio/config/saudeTerritorio.module#SaudeTerritorioModule',
    canActivateChild: [AuthGuardService]
  },
  {
    path: 'iptu',
    loadChildren: './iptu/config/iptu.module#IptuModule',
    canActivateChild: [AuthGuardService]
  },
  {
    path: 'notAuthorized',
    component: ForbiddenComponent
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
