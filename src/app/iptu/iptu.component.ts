import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import {MatInputModule} from '@angular/material/input'; 
import { MatDialog } from '@angular/material';
import { Observable, BehaviorSubject } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { Component, OnInit } from '@angular/core';
import { IptuService } from './services/iptu.service';
import { ImoveisModalComponent } from './modais/imoveis-modal.component';

@Component({
  selector: 'app-iptu',
  templateUrl: './iptu.component.html',
  styleUrls: ['./iptu.component.css']
})
export class IptuComponent implements OnInit {

  private lastSearch: string;
  protected form: FormGroup;
  protected maxDate: Date = new Date();
  protected dataSource: Observable<ProprietarioModel[]>;
  protected behaviorSubject: BehaviorSubject<ProprietarioModel[]> = new BehaviorSubject<ProprietarioModel[]>(null);
  protected displayedColumns: string[] = ['numero_contribuinte','tipo_contribuinte','documento','imoveis'];

  constructor(protected iptuService: IptuService, protected fb: FormBuilder, protected customDialog: MatDialog) {
    this.form = this.fb.group({
      cpf: new FormControl()
    });
    this.dataSource = this.behaviorSubject.asObservable();
  }

  ngOnInit() {

    this.searchProprietarios();

    this.form.get('documento').valueChanges.subscribe(documento => {
      if (!isNullOrUndefined(documento) &&  documento != this.lastSearch) {
        this.lastSearch = documento;
        this.searchProprietarios(documento);
      } else if (isNullOrUndefined(documento) || (documento as string).length === 0) {
        this.lastSearch = documento;
        this.searchProprietarios(documento);
      }
    });
  
  };
  private searchProprietarios(documento?: string) {
    this.iptuService.searchProprietarios(documento).subscribe((data: ProprietarioModel[]) => {
      if (!isNullOrUndefined(data)) {
        this.behaviorSubject.next(data);
      } else {
        this.behaviorSubject.next([]);
      }
    });
  }
  public detalhesImovel(imovel: ImovelModel) {

    const dialogRef = this.customDialog.open(
      ImoveisModalComponent
      , { data: imovel }
    );

    
  }

}
