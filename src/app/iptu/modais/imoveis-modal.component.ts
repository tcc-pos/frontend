import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'imoveis-modal',
    templateUrl: './imoveis-modal.component.html',
    styleUrls: ['./imoveis-modal.component.css']
})
export class ImoveisModalComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<ImoveisModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ImovelModel, protected fb: FormBuilder) {

        this.form = this.fb.group({
            id: new FormControl(data.id, [Validators.required])
            , codigo_cadastro: new FormControl(data.codigo_cadastro, [Validators.required])
            , logradouro: new FormControl(data.logradouro, [Validators.required])
            , numero: new FormControl(data.numero, [Validators.required])
            , complemento: new FormControl(data.complemento, [Validators.required])
            , bairro: new FormControl(data.bairro , [Validators.required])
            , cep: new FormControl(data.cep , [Validators.required])
            , uso: new FormControl(data.uso  , [Validators.required])
            , area_terreno: new FormControl( data.area_terreno , [Validators.required])
            , area_construida: new FormControl( data.area_construida , [Validators.required])
            , area_ocupada: new FormControl(data.area_ocupada , [Validators.required])
            , valor_construida: new FormControl(data.valor_construida , [Validators.required])
            , tableRows: this.fb.array([])
        });

        data.impostos.forEach(i => {
            this.addRow(i);
        });

        dialogRef.disableClose = true;
    }

    public getFormControls() {
        const control = this.form.get('tableRows') as FormArray;
        return control;
    }

    public addRow(imposto?: ImpostoModel) {
        const control = this.form.get('tableRows') as FormArray;
        control.push(this.initiateImpostosForm(imposto));
    }

    public initiateImpostosForm(imposto?: ImpostoModel): FormGroup {
        return this.fb.group({
            id: new FormControl((imposto !== undefined && imposto !== null) ? imposto.id : null, [Validators.required]),
            numero_documento: new FormControl((imposto !== undefined && imposto !== null) ? imposto.numero_documento : null, [Validators.required]),
            data_base: new FormControl((imposto !== undefined && imposto !== null) ? imposto.data_base : null, [Validators.required]),
            vencimento_parcela1: new FormControl((imposto !== undefined && imposto !== null) ? imposto.vencimento_parcelas[0] : null, [Validators.required]),
            vencimento_parcela2: new FormControl((imposto !== undefined && imposto !== null) ? imposto.vencimento_parcelas[1] : null, [Validators.required]),
            vencimento_parcela3: new FormControl((imposto !== undefined && imposto !== null) ? imposto.vencimento_parcelas[2] : null, [Validators.required]),
            vencimento_parcela4: new FormControl((imposto !== undefined && imposto !== null) ? imposto.vencimento_parcelas[3] : null, [Validators.required]),
            total: new FormControl((imposto !== undefined && imposto !== null) ? imposto.total : null, [Validators.required]),
            });
    }

    private getDataFromForm(): ImovelModel {
        const imovel = {} as ImovelModel;
        imovel.id = this.form.get('id').value;
        imovel.codigo_cadastro = this.form.get('codigo_cadastro').value;
        imovel.logradouro = this.form.get('logradouro').value;
        imovel.numero = this.form.get('numero').value;
        imovel.complemento = this.form.get('complemento').value;
        imovel.bairro = this.form.get('bairro').value;
        imovel.cep = this.form.get('cep').value;
        imovel.uso = this.form.get('uso').value;
        imovel.area_terreno = this.form.get('area_terreno').value;
        imovel.area_construida = this.form.get('area_construida').value;
        imovel.area_ocupada = this.form.get('area_ocupada').value;
        imovel.valor_construida = this.form.get('valor_construida').value;

        imovel.impostos = [];
        (this.form.get('tableRows') as FormArray).controls.forEach(i => {
            const item = {} as ImpostoModel;
            item.id = i.get('id').value;
            item.numero_documento = i.get('numero_documento').value;
            item.data_base = i.get('data_base').value;
            item.vencimento_parcelas = i.get('data_base').value;
            item.total = i.get('total').value;
            
            imovel.impostos.push(item);
        });

        return imovel;
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}