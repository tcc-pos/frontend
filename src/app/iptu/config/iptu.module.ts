import { NgModule } from '@angular/core';

import { IptuRoutingModule } from './iptu-routing.module';
import { IptuComponent } from '../iptu.component';
import { MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatTableModule, MatTabsModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { ImoveisModalComponent } from '../modais/imoveis-modal.component';


@NgModule({
    declarations: [
        IptuComponent,
        ImoveisModalComponent
    ],
    imports: [
        IptuRoutingModule,
        MatTableModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatTabsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        CommonModule,
        NgxMaskModule.forRoot()
    ],
    exports: [
        IptuComponent
    ],
    entryComponents: [
        ImoveisModalComponent
        
    ]
})
export class IptuModule { }