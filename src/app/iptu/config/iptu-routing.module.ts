import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IptuComponent } from '../iptu.component';

const routes: Routes = [
    {
        path: '',
        component: IptuComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IptuRoutingModule { }