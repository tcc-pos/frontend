interface ImovelModel {
    id: number;
    codigo_cadastro: string;
    logradouro: string;
    numero: string;
    complemento: string;
    bairro: string;
    cep: string;
    uso: string;
    area_terreno: string;
    area_construida: string;
    area_ocupada: string;
    valor_construida: string;
    impostos: ImpostoModel[];
    
}