interface ImpostoModel {
    id: number;
    numero_documento: string;
    data_base: string;
    vencimento_parcelas: string[];
    total: string;
}