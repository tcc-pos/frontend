interface ProprietarioModel {
    id: number;
    numero_contribuinte: string;
    documento: string;
    tipo_documento: string;
    nome: string;
    tipo_contribuinte: string;
    data_cadastramento: string;
    imoveis: ImovelModel[];
}