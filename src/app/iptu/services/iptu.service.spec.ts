import { TestBed } from '@angular/core/testing';

import { IptuService } from './iptu.service';

describe('IptuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IptuService = TestBed.get(IptuService);
    expect(service).toBeTruthy();
  });
});
