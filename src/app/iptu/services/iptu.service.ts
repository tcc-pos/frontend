
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from 'src/app/user.service';


@Injectable({
  providedIn: 'root'
})
export class IptuService {

  constructor(protected http: HttpClient, protected userService: UserService) { }

  public searchProprietarios(documento?: string): Observable<ProprietarioModel[]> {
    let params = {};

    if (documento !== undefined && documento !== null) {
      params = { documento: documento };
    }else {
      params = { documento : this.userService.getSub() };
    }

    return this.http.get(`/api/impostos/proprietarios/`, {
      params: params,
    }).pipe(map(value => value as ProprietarioModel[]));
  }

  public searchProprietarioById(id: number): Observable<ProprietarioModel> {
    return this.http.get(`/api/impostos/proprietarios/${id}`).pipe(map(value => value as ProprietarioModel));
  }
}
