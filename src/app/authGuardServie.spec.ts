import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AuthGuardService } from './authGuardService';

describe('AuthGuardService', () => {
  let component: AuthGuardService;
  let fixture: ComponentFixture<AuthGuardService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthGuardService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthGuardService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});