import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatSidenavModule, MatIconModule, MatButtonModule, MAT_DATE_LOCALE } from '@angular/material';
import { NotFoundComponent } from './notFoundComponent';
import { AuthGuardService } from './authGuardService';
import { ForbiddenComponent } from './forbiddenComponent';
import { MyHttpInterceptor } from './myHttpInterceptor';
import { StorageServiceModule } from 'ngx-webstorage-service';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    ForbiddenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    StorageServiceModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
    AuthGuardService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
