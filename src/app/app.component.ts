import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'TCC';
  public isMenuOpen: boolean;
  

  public constructor(protected userService: UserService) {

  }

  public logout(): void {
    this.userService.logout();
  }

  public onSidenavClick(userService: UserService): void {
    this.isMenuOpen = false;
  }
}
