import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {

    private routesOnlyAdmin: string[] = [];
    private ADMIN: string = "ADMIN";

    constructor(private router: Router, protected userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.userService.getIsLoggedIn()) {
            this.router.navigateByUrl('/notAuthorized');
        }

        if (this.routesOnlyAdmin.indexOf(state.url) == 0 && this.userService.getAuthorities().indexOf("ADMIN") == -1) {
            this.router.navigateByUrl('/notAuthorized');
        }

        return this.userService.getIsLoggedIn();
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        if (!this.userService.getIsLoggedIn()) {
            this.router.navigateByUrl('/notAuthorized');
        }

        if (this.routesOnlyAdmin.indexOf(state.url) == 0 && this.userService.getAuthorities().indexOf("ADMIN") == -1) {
            this.router.navigateByUrl('/notAuthorized');
        }

        return this.userService.getIsLoggedIn();
    }

}