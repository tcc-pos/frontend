import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AlunoService } from '../../services/aluno.service';

@Component({
    selector: 'aluno-modal-edit',
    templateUrl: './aluno-modal-edit.component.html',
    styleUrls: ['./aluno-modal-edit.component.css']
})
export class AlunoModalEditComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<AlunoModalEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: AlunoModel, protected alunoService: AlunoService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            matricula: new FormControl(data.matricula, [Validators.required])
            , nome: new FormControl(data.nome, [Validators.required])
            , cpfAluno: new FormControl(data.cpfAluno, [Validators.required])
            , serieAtual: new FormControl(data.serieAtual, [Validators.required])
            , ano: new FormControl(data.ano, [Validators.required])
            , ch: new FormControl(data.ch, [Validators.required])
            , totalDias: new FormControl(data.totalDias, [Validators.required])
            , faltas: new FormControl(data.faltas, [Validators.required])
            , cpfResponsavel: new FormControl(data.responsavel.cpfResponsavel, [Validators.required])
            , nomeResponsavel: new FormControl(data.responsavel.nome, [Validators.required])
            , nascimentoResponsavel: new FormControl(data.responsavel.nascimento, [Validators.required])
            , unidadeEnsino: new FormControl(data.unidadeEnsino.unidadeEnsino, [Validators.required])
            , enderecoUE: new FormControl(data.unidadeEnsino.enderecoUE, [Validators.required])
            , cepUE: new FormControl(data.unidadeEnsino.cepUE, [Validators.required])
            , tableRows: this.fb.array([])
        });

        data.componenteCurricular.forEach(i => {
            this.addRow(i);
        });

        dialogRef.disableClose = true;
    }

    public getFormControls() {
        const control = this.form.get('tableRows') as FormArray;
        return control;
    }

    public addRow(materia?: MateriaModel) {
        const control = this.form.get('tableRows') as FormArray;
        control.push(this.initiateNotasForm(materia));
    }

    public deleteRow(index: number) {
        const control = this.form.get('tableRows') as FormArray;
        control.removeAt(index);
    }

    public initiateNotasForm(materia?: MateriaModel): FormGroup {
        return this.fb.group({
            materiaNome: new FormControl((materia !== undefined && materia !== null) ? materia.nome : null, [Validators.required]),
            nota1: new FormControl((materia !== undefined && materia !== null) ? materia.nota1 : null, [Validators.required]),
            nota2: new FormControl((materia !== undefined && materia !== null) ? materia.nota2 : null),
            nota3: new FormControl((materia !== undefined && materia !== null) ? materia.nota3 : null),
            nota4: new FormControl((materia !== undefined && materia !== null) ? materia.nota4 : null)
        });
    }

    private getDataFromForm(): AlunoModel {
        const newAluno = {} as AlunoModel;
        newAluno.matricula = this.form.get('matricula').value;
        newAluno.nome = this.form.get('nome').value;
        newAluno.cpfAluno = this.form.get('cpfAluno').value;
        newAluno.serieAtual = this.form.get('serieAtual').value;
        newAluno.ano = this.form.get('ano').value;
        newAluno.ch = this.form.get('ch').value;
        newAluno.totalDias = this.form.get('totalDias').value;
        newAluno.faltas = this.form.get('faltas').value;
        newAluno.responsavel = {} as ResponsavelModel;
        newAluno.responsavel.cpfResponsavel = this.form.get('cpfResponsavel').value;
        newAluno.responsavel.nome = this.form.get('nomeResponsavel').value;
        newAluno.responsavel.nascimento = this.form.get('nascimentoResponsavel').value;
        newAluno.unidadeEnsino = {} as UnidadeEnsinoModel;
        newAluno.unidadeEnsino.unidadeEnsino = this.form.get('unidadeEnsino').value;
        newAluno.unidadeEnsino.enderecoUE = this.form.get('enderecoUE').value;
        newAluno.unidadeEnsino.cepUE = this.form.get('cepUE').value;

        newAluno.componenteCurricular = [];
        (this.form.get('tableRows') as FormArray).controls.forEach(i => {
            const item = {} as MateriaModel;
            item.nome = i.get('materiaNome').value;
            item.nota1 = i.get('nota1').value;
            item.nota2 = i.get('nota2').value;
            item.nota3 = i.get('nota3').value;
            item.nota4 = i.get('nota4').value;
            newAluno.componenteCurricular.push(item);
        });

        return newAluno;
    }

    protected onOkClick(): void {
        const newAluno = this.getDataFromForm();
        newAluno.id = this.data.id;
        this.alunoService.updateAluno(newAluno).subscribe(
            (success) => {
                this.dialogRef.close(success);
            },
            (fail) => {
                console.log(fail);
                this.dialogRef.close();
            }
        );
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}