import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AlunoService } from '../../services/aluno.service';

@Component({
    selector: 'aluno-modal-add',
    templateUrl: './aluno-modal-add.component.html',
    styleUrls: ['./aluno-modal-add.component.css']
})
export class AlunoModalAddComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<AlunoModalAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: AlunoModel, protected alunoService: AlunoService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            matricula: new FormControl(null, [Validators.required])
            , nome: new FormControl(null, [Validators.required])
            , cpfAluno: new FormControl(null, [Validators.required])
            , serieAtual: new FormControl(null, [Validators.required])
            , ano: new FormControl(null, [Validators.required])
            , ch: new FormControl(null, [Validators.required])
            , totalDias: new FormControl(null, [Validators.required])
            , faltas: new FormControl(null, [Validators.required])
            , cpfResponsavel: new FormControl(null, [Validators.required])
            , nomeResponsavel: new FormControl(null, [Validators.required])
            , nascimentoResponsavel: new FormControl(null, [Validators.required])
            , unidadeEnsino: new FormControl(null, [Validators.required])
            , enderecoUE: new FormControl(null, [Validators.required])
            , cepUE: new FormControl(null, [Validators.required])
            , tableRows: this.fb.array([])
        });

        dialogRef.disableClose = true;
    }

    public getFormControls() {
        const control = this.form.get('tableRows') as FormArray;
        return control;
    }

    public addRow(materia?: MateriaModel) {
        const control = this.form.get('tableRows') as FormArray;
        control.push(this.initiateNotasForm(materia));
    }

    public deleteRow(index: number) {
        const control = this.form.get('tableRows') as FormArray;
        control.removeAt(index);
    }

    public initiateNotasForm(materia?: MateriaModel): FormGroup {
        return this.fb.group({
            materiaNome: new FormControl(null, [Validators.required]),
            nota1: new FormControl(null, [Validators.required]),
            nota2: new FormControl(),
            nota3: new FormControl(),
            nota4: new FormControl()
        });
    }

    private getDataFromForm(): AlunoModel {
        const newAluno = {} as AlunoModel;
        newAluno.matricula = this.form.get('matricula').value;
        newAluno.nome = this.form.get('nome').value;
        newAluno.cpfAluno = this.form.get('cpfAluno').value;
        newAluno.serieAtual = this.form.get('serieAtual').value;
        newAluno.ano = this.form.get('ano').value;
        newAluno.ch = this.form.get('ch').value;
        newAluno.totalDias = this.form.get('totalDias').value;
        newAluno.faltas = this.form.get('faltas').value;
        newAluno.responsavel = {} as ResponsavelModel;
        newAluno.responsavel.cpfResponsavel = this.form.get('cpfResponsavel').value;
        newAluno.responsavel.nome = this.form.get('nomeResponsavel').value;
        newAluno.responsavel.nascimento = this.form.get('nascimentoResponsavel').value;
        newAluno.unidadeEnsino = {} as UnidadeEnsinoModel;
        newAluno.unidadeEnsino.unidadeEnsino = this.form.get('unidadeEnsino').value;
        newAluno.unidadeEnsino.enderecoUE = this.form.get('enderecoUE').value;
        newAluno.unidadeEnsino.cepUE = this.form.get('cepUE').value;

        newAluno.componenteCurricular = [];
        (this.form.get('tableRows') as FormArray).controls.forEach(i => {
            const item = {} as MateriaModel;
            item.nome = i.get('materiaNome').value;
            item.nota1 = i.get('nota1').value;
            item.nota2 = i.get('nota2').value;
            item.nota3 = i.get('nota3').value;
            item.nota4 = i.get('nota4').value;
            newAluno.componenteCurricular.push(item);
        });

        return newAluno;
    }

    protected onOkClick(): void {
        this.alunoService.createAluno(this.getDataFromForm()).subscribe(
            (success) => {
                this.dialogRef.close(success);
            },
            (fail) => {
                console.log(fail);
                this.dialogRef.close();
            }
        );
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}