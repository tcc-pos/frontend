import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AlunoService } from '../../services/aluno.service';

@Component({
    selector: 'aluno-modal-delete',
    templateUrl: './aluno-modal-delete.component.html',
    styleUrls: ['./aluno-modal-delete.component.css']
})
export class AlunoModalDeleteComponent {

    constructor(
        public dialogRef: MatDialogRef<AlunoModalDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) public data: AlunoModel, protected alunoService: AlunoService) {
        dialogRef.disableClose = true;
    }

    protected onOkClick(): void {
        this.alunoService.deleteAluno(this.data.id).subscribe(
            (success) => {
                this.dialogRef.close(this.data);
            },
            (fail) => {
                console.log(fail);
                this.dialogRef.close();
            }
        );
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}