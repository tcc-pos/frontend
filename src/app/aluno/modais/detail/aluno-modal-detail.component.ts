import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'aluno-modal-detail',
    templateUrl: './aluno-modal-detail.component.html',
    styleUrls: ['./aluno-modal-detail.component.css']
})
export class AlunoModalDetailComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<AlunoModalDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: AlunoModel, protected fb: FormBuilder) {

        this.form = this.fb.group({
            matricula: new FormControl({ value: data.matricula, disabled: true }, [Validators.required])
            , nome: new FormControl({ value: data.nome, disabled: true }, [Validators.required])
            , cpfAluno: new FormControl({ value: data.cpfAluno, disabled: true }, [Validators.required])
            , serieAtual: new FormControl({ value: data.serieAtual, disabled: true }, [Validators.required])
            , faltas: new FormControl({ value: data.faltas, disabled: true }, [Validators.required])
            , cpfResponsavel: new FormControl({ value: data.cpfResponsavel, disabled: true }, [Validators.required])
            , unidadeEnsino: new FormControl({ value: data.unidadeEnsino.unidadeEnsino, disabled: true }, [Validators.required])
            , tableRows: this.fb.array([])
        });

        data.componenteCurricular.forEach(i => {
            this.addRow(i);
        });

        dialogRef.disableClose = true;
    }

    public addRow(materia?: MateriaModel) {
        const control = this.form.get('tableRows') as FormArray;
        control.push(this.initiateNotasForm(materia));
    }

    public getFormControls() {
        const control = this.form.get('tableRows') as FormArray;
        return control;
    }

    public initiateNotasForm(materia?: MateriaModel): FormGroup {
        return this.fb.group({
            materiaNome: new FormControl((materia !== undefined && materia !== null) ? { value: materia.nome, disabled: true } : null, [Validators.required]),
            nota1: new FormControl((materia !== undefined && materia !== null) ? { value: materia.nota1, disabled: true } : null, [Validators.required]),
            nota2: new FormControl((materia !== undefined && materia !== null) ? { value: materia.nota2, disabled: true } : null),
            nota3: new FormControl((materia !== undefined && materia !== null) ? { value: materia.nota3, disabled: true } : null),
            nota4: new FormControl((materia !== undefined && materia !== null) ? { value: materia.nota4, disabled: true } : null)
        });
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}