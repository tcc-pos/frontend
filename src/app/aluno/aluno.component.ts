import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { BehaviorSubject, Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { AlunoModalAddComponent } from './modais/add/aluno-modal-add.component';
import { AlunoModalDeleteComponent } from './modais/delete/aluno-modal-delete.component';
import { AlunoModalDetailComponent } from './modais/detail/aluno-modal-detail.component';
import { AlunoModalEditComponent } from './modais/edit/aluno-modal-edit.component';
import { AlunoService } from './services/aluno.service';

@Component({
  selector: 'app-aluno',
  templateUrl: './aluno.component.html',
  styleUrls: ['./aluno.component.css']
})
export class AlunoComponent implements OnInit {

  private lastSearch: number;
  protected form: FormGroup;
  protected dataSource: Observable<AlunoModel[]>;
  protected behaviorSubject: BehaviorSubject<AlunoModel[]> = new BehaviorSubject<AlunoModel[]>(null);
  //protected displayedColumns: string[] = ['matricula', 'nome', 'nascimento', 'cpfAluno', 'serieAtual', 'ano', 'ch', 'totalDias', 'faltas', 'actions'];
  protected displayedColumns: string[] = ['matricula', 'nome', 'nascimento', 'cpfAluno', 'serieAtual', 'ano', 'faltas', 'actions'];

  constructor(protected alunoService: AlunoService, protected fb: FormBuilder, protected customDialog: MatDialog) {
    this.form = this.fb.group({
      cpfResponsavel: new FormControl()
    });
    this.dataSource = this.behaviorSubject.asObservable();
  }

  ngOnInit() {
    this.searchAluno();

    this.form.get('cpfResponsavel').valueChanges.subscribe(cpf => {
      if (!isNullOrUndefined(cpf) && (cpf as string).length === 11 && cpf != this.lastSearch) {
        this.lastSearch = cpf;
        this.searchAluno(cpf);
      } else if (isNullOrUndefined(cpf) || (cpf as string).length === 0) {
        this.lastSearch = cpf;
        this.searchAluno(cpf);
      }
    });
  }

  private searchAluno(cpf?: string) {
    this.alunoService.searchAluno(cpf).subscribe((data: AlunoModel[]) => {
      if (!isNullOrUndefined(data)) {
        this.behaviorSubject.next(data);
      } else {
        this.behaviorSubject.next([]);
      }
    });
  }

  public detail(aluno: AlunoModel) {
    const dialogRef = this.customDialog.open(
      AlunoModalDetailComponent
      , { data: aluno }
    );
  }

  public edit(aluno: AlunoModel) {
    const dialogRef = this.customDialog.open(
      AlunoModalEditComponent
      , { data: aluno }
    );

    dialogRef.afterClosed().subscribe((result: AlunoModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.getValue().forEach(i => {
          if(i.id === result.id) {
            Object.assign(i, result);
          }
        });
      }
    });
  }

  public add() {
    const dialogRef = this.customDialog.open(
      AlunoModalAddComponent
    );

    dialogRef.afterClosed().subscribe((result: AlunoModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.getValue().push(result);
        this.behaviorSubject.next(this.behaviorSubject.getValue());
      }
    });
  }

  public delete(aluno: AlunoModel) {
    const dialogRef = this.customDialog.open(
      AlunoModalDeleteComponent
      , { data: aluno }
    );

    dialogRef.afterClosed().subscribe((result: AlunoModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.next(this.behaviorSubject.getValue().filter((aluno: AlunoModel) => {
          return aluno.id != result.id;
        }));
      }
    });
  }

}
