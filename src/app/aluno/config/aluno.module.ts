import { NgModule } from '@angular/core';

import { AlunoRoutingModule } from './aluno-routing.module';
import { AlunoComponent } from '../aluno.component';
import { MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatTableModule, MatTabsModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { AlunoModalDeleteComponent } from '../modais/delete/aluno-modal-delete.component';
import { AlunoModalEditComponent } from '../modais/edit/aluno-modal-edit.component';
import { CommonModule } from '@angular/common';
import { AlunoModalAddComponent } from '../modais/add/aluno-modal-add.component';
import { AlunoModalDetailComponent } from '../modais/detail/aluno-modal-detail.component';

@NgModule({
    declarations: [
        AlunoComponent,
        AlunoModalDeleteComponent,
        AlunoModalEditComponent,
        AlunoModalAddComponent,
        AlunoModalDetailComponent
    ],
    imports: [
        AlunoRoutingModule,
        MatTableModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatTabsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        CommonModule,
        NgxMaskModule.forRoot()
    ],
    exports: [
        AlunoComponent
    ],
    entryComponents: [
        AlunoModalDeleteComponent,
        AlunoModalEditComponent,
        AlunoModalAddComponent,
        AlunoModalDetailComponent
    ]
})
export class AlunoModule { }