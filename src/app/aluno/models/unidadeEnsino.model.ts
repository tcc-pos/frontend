interface UnidadeEnsinoModel {
    codUE: number;
    unidadeEnsino: string;
    enderecoUE: string;
    cepUE: string;
}