interface AlunoModel {

    id: number;
	matricula: string;
	nome: string;
	nascimento: string;
	cpfAluno: string;
	cpfResponsavel: string;
	serieAtual: number;
	ano: string;
	ch: number;
	totalDias: number;
	faltas: number;
	responsavel: ResponsavelModel;
	unidadeEnsino: UnidadeEnsinoModel;
	componenteCurricular: MateriaModel[];

}