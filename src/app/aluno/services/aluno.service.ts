
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from 'src/app/user.service';

@Injectable({
  providedIn: 'root'
})
export class AlunoService {

  constructor(protected http: HttpClient, protected userService: UserService) { }

  public searchAluno(cpf?: string): Observable<AlunoModel[]> {
    let params = {};

    if (cpf !== undefined && cpf !== null) {
      params = { cpf: cpf };
    } else {
      params = { cpf : this.userService.getSub() };
    }

    return this.http.get(`/api/saem/alunos`, {
      params: params,
    }).pipe(map(value => value as AlunoModel[]));
  }

  public searchAlunoById(id: number): Observable<AlunoModel> {
    return this.http.get(`/api/saem/alunos/${id}`).pipe(map(value => value as AlunoModel));
  }

  public createAluno(aluno: AlunoModel): Observable<AlunoModel> {
    return this.http.post(`/api/saem/alunos`, aluno).pipe(map(value => value as AlunoModel));
  }

  public updateAluno(aluno: AlunoModel): Observable<AlunoModel> {
    return this.http.put(`/api/saem/alunos/${aluno.id}`, aluno).pipe(map(value => value as AlunoModel));
  }

  public deleteAluno(id: number): Observable<void> {
    return this.http.delete(`/api/saem/alunos/${id}`).pipe(map(value => value as unknown as void));
  }

}
