
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConsultaMedicaService {

  constructor(protected http: HttpClient) { }

  public searchConsultaMedica(cpf?: string): Observable<PacienteModel[]> {
    let params = {};

    if (cpf !== undefined && cpf !== null) {
      params = { cpf: cpf };
    }

    return this.http.get(`/api/consultas-medicas/pacientes`, {
      params: params,
    }).pipe(map(value => value as PacienteModel[]));
  }

  public searchConsultaMedicaById(cpf: string): Observable<PacienteModel> {
    return this.http.get(`/api/consultas-medicas/pacientes/${cpf}`).pipe(map(value => value as PacienteModel));
  }

  public createConsultaMedica(paciente: PacienteModel): Observable<PacienteModel> {
    return this.http.post(`/api/consultas-medicas/pacientes`, paciente).pipe(map(value => value as PacienteModel));
  }

  public updateConsultaMedica(paciente: PacienteModel): Observable<PacienteModel> {
    return this.http.put(`/api/consultas-medicas/pacientes/${paciente.cpf}`, paciente).pipe(map(value => value as PacienteModel));
  }

  public deleteConsultaMedica(cpf: string): Observable<void> {
    return this.http.delete(`/api/consultas-medicas/pacientes/${cpf}`).pipe(map(value => value as unknown as void));
  }

}