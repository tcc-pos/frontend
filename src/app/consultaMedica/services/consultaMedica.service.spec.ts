import { TestBed } from '@angular/core/testing';

import { ConsultaMedicaService } from './consultaMedica.service';

describe('ConsultaMedicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsultaMedicaService = TestBed.get(ConsultaMedicaService);
    expect(service).toBeTruthy();
  });
});
