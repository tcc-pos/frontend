import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConsultaMedicaService } from '../../services/consultaMedica.service';

@Component({
    selector: 'consultaMedica-modal-consultas',
    templateUrl: './consultaMedica-modal-consultas.component.html',
    styleUrls: ['./consultaMedica-modal-consultas.component.css']
})
export class ConsultaMedicaConsultasComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<ConsultaMedicaConsultasComponent>,
        @Inject(MAT_DIALOG_DATA) public data: PacienteModel, protected consultaMedicaService: ConsultaMedicaService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            cpfPaciente: new FormControl(data.cpf, [Validators.required])
            , codSus: new FormControl(data.cns, [Validators.required])
            , nome: new FormControl(data.nome, [Validators.required])
            , nascimento: new FormControl(data.nascimento, [Validators.required])
            , tableRows: this.fb.array([])
        });

        data.consultas.forEach(i => {
            this.addRow(i);
        });

        dialogRef.disableClose = true;
    }

    public getFormControls() {
        const control = this.form.get('tableRows') as FormArray;
        return control;
    }

    public addRow(consulta?: ConsultaModel) {
        const control = this.form.get('tableRows') as FormArray;
        control.push(this.initiateConsultasForm(consulta));
    }

    public initiateConsultasForm(consulta?: ConsultaModel): FormGroup {
        return this.fb.group({
            data: new FormControl((consulta !== undefined && consulta !== null) ? consulta.data : null, [Validators.required]),
            motivoConsulta: new FormControl((consulta !== undefined && consulta !== null) ? consulta.motivoConsulta : null, [Validators.required]),
            especialidade: new FormControl((consulta !== undefined && consulta !== null) ? consulta.especialidade : null, [Validators.required]),
            unidadeSaude: new FormControl((consulta !== undefined && consulta !== null) ? consulta.unidadeSaude.nomeFantasia : null, [Validators.required]),
            endereco: new FormControl((consulta !== undefined && consulta !== null) ? consulta.unidadeSaude.endereco : null, [Validators.required]),
            cep: new FormControl((consulta !== undefined && consulta !== null) ? consulta.unidadeSaude.cep : null, [Validators.required]),
            medico: new FormControl((consulta !== undefined && consulta !== null) ? consulta.medico.nome : null, [Validators.required]),
            crm: new FormControl((consulta !== undefined && consulta !== null) ? consulta.medico.crm : null, [Validators.required]),
            cpfMedico: new FormControl((consulta !== undefined && consulta !== null) ? consulta.medico.cpf : null, [Validators.required]),
            nascimentoMedico: new FormControl((consulta !== undefined && consulta !== null) ? consulta.medico.nascimento : null, [Validators.required])
        });
    }

    private getDataFromForm(): PacienteModel {
        const paciente = {} as PacienteModel;
        paciente.cpf = this.form.get('cpf').value;
        paciente.cns = this.form.get('cnes').value;
        paciente.nome = this.form.get('nome').value;
        paciente.nascimento = this.form.get('nascimento').value;

        paciente.consultas = [];
        (this.form.get('tableRows') as FormArray).controls.forEach(i => {
            const item = {} as ConsultaModel;
            item.data = i.get('data').value;
            item.hipoteseDiagnostico = i.get('hipoteseDiagnostico').value;
            item.especialidade = i.get('especialidade').value;
            item.unidadeSaude = {} as UnidadeSaudeModel;
            item.unidadeSaude.nome = i.get('unidadeSaude').value;
            item.unidadeSaude.endereco = i.get('endereco').value;
            item.unidadeSaude.cep = i.get('cep').value;
            item.medico = {} as MedicoModel;
            item.medico.nome = i.get('medico').value;
            item.medico.crm = i.get('crm').value;
            item.medico.cpf = i.get('cpfMedico').value;
            item.medico.nascimento = i.get('nascimentoMedico').value;
            paciente.consultas.push(item);
        });

        return paciente;
    }

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}