import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConsultaMedicaService } from '../../services/consultaMedica.service';

@Component({
    selector: 'consultaMedica-modal-exames',
    templateUrl: './consultaMedica-modal-exames.component.html',
    styleUrls: ['./consultaMedica-modal-exames.component.css']
})
export class ConsultaMedicaExamesComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<ConsultaMedicaExamesComponent>,
        @Inject(MAT_DIALOG_DATA) public data: PacienteModel, protected consultaMedicaService: ConsultaMedicaService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            cpfPaciente: new FormControl(data.cpf, [Validators.required])
            , codSus: new FormControl(data.cns, [Validators.required])
            , nome: new FormControl(data.nome, [Validators.required])
            , nascimento: new FormControl(data.nascimento, [Validators.required])
            , tableRows: this.fb.array([])
        });

        data.exames.forEach(i => {
            this.addRow(i);
        });

        dialogRef.disableClose = true;
    }

    public getFormControls() {
        const control = this.form.get('tableRows') as FormArray;
        return control;
    }

    public addRow(exame?: ExameProcedimentoModel) {
        const control = this.form.get('tableRows') as FormArray;
        control.push(this.initiateExamesForm(exame));
    }

    public deleteRow(index: number) {
        const control = this.form.get('tableRows') as FormArray;
        control.removeAt(index);
    }

    public initiateExamesForm(exame?: ExameProcedimentoModel): FormGroup {
        return this.fb.group({
            data: new FormControl((exame !== undefined && exame !== null) ? exame.data : null, [Validators.required]),
            id: new FormControl((exame !== undefined && exame !== null) ? exame.id : null, [Validators.required]),
            grupo: new FormControl((exame !== undefined && exame !== null) ? exame.grupo : null, [Validators.required]),
            subGrupo: new FormControl((exame !== undefined && exame !== null) ? exame.subGrupo : null, [Validators.required]),
            nome: new FormControl((exame !== undefined && exame !== null) ? exame.nome : null, [Validators.required]),
            });
    }

    private getDataFromForm(): PacienteModel {
        const paciente = {} as PacienteModel;
        paciente.cpf = this.form.get('cpf').value;
        paciente.cns = this.form.get('cnes').value;
        paciente.nome = this.form.get('nome').value;
        paciente.nascimento = this.form.get('nascimento').value;

        paciente.exames = [];
        (this.form.get('tableRows') as FormArray).controls.forEach(i => {
            const item = {} as ExameProcedimentoModel;
            item.data = i.get('data').value;
            item.id = i.get('id').value;
            item.grupo = i.get('grupo').value;
            item.subGrupo = i.get('subGrupo').value;
            item.nome = i.get('nome').value;
            paciente.exames.push(item);
        });

        return paciente;
    }

    

    protected onNoClick(): void {
        this.dialogRef.close();
    }

}