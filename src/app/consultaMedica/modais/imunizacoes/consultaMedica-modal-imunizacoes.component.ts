import { Component, Inject } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConsultaMedicaService } from '../../services/consultaMedica.service';

@Component({
    selector: 'consultaMedica-modal-imunizacoes',
    templateUrl: './consultaMedica-modal-imunizacoes.component.html',
    styleUrls: ['./consultaMedica-modal-imunizacoes.component.css']
})
export class ConsultaMedicaImunizacoesComponent {

    protected form: FormGroup;
    protected maxDate: Date = new Date();

    constructor(
        public dialogRef: MatDialogRef<ConsultaMedicaImunizacoesComponent>,
        @Inject(MAT_DIALOG_DATA) public data: PacienteModel, protected consultaMedicaService: ConsultaMedicaService, protected fb: FormBuilder) {

        this.form = this.fb.group({
            cpfPaciente: new FormControl(data.cpf, [Validators.required])
            , codSus: new FormControl(data.cns, [Validators.required])
            , nome: new FormControl(data.nome, [Validators.required])
            , nascimento: new FormControl(data.nascimento, [Validators.required])
            , tableRows: this.fb.array([])
        });

        data.imunizacoes.forEach(i => {
            this.addRow(i);
        });

        dialogRef.disableClose = true;
    }

    public getFormControls() {
        const control = this.form.get('tableRows') as FormArray;
        return control;
    }

    public addRow(imunizacao?: ImunizacaoModel) {
        const control = this.form.get('tableRows') as FormArray;
        control.push(this.initiateImunizacoesForm(imunizacao));
    }


    public initiateImunizacoesForm(imunizacao?: ImunizacaoModel): FormGroup {
        return this.fb.group({
            codigo: new FormControl((imunizacao !== undefined && imunizacao !== null) ? imunizacao.id : null, [Validators.required]),
            data: new FormControl((imunizacao !== undefined && imunizacao !== null) ? imunizacao.dataAdministracao : null, [Validators.required]),
            dose: new FormControl((imunizacao !== undefined && imunizacao !== null) ? imunizacao.dose : null, [Validators.required]),
            nomeImunobiologico: new FormControl((imunizacao !== undefined && imunizacao !== null) ? imunizacao.nomeImunobiologico : null, [Validators.required]),
             });
    }

    private getDataFromForm(): PacienteModel {
        const paciente = {} as PacienteModel;
        paciente.cpf = this.form.get('cpf').value;
        paciente.cns = this.form.get('cns').value;
        paciente.nome = this.form.get('nome').value;
        paciente.nascimento = this.form.get('nascimento').value;

        paciente.imunizacoes = [];
        (this.form.get('tableRows') as FormArray).controls.forEach(i => {
            const item = {} as ImunizacaoModel;
            item.dataAdministracao = i.get('dataAdministracao').value;
            item.id = i.get('id').value;
            item.dose = i.get('dose').value;
            item.nomeImunobiologico = i.get('nomeImunobiologico').value;
            paciente.imunizacoes.push(item);
        });

        return paciente;
    }


    protected onNoClick(): void {
        this.dialogRef.close();
    }

}