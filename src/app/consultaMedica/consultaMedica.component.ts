import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import {MatInputModule} from '@angular/material/input'; 
import { MatDialog } from '@angular/material';
import { Observable, BehaviorSubject } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { ConsultaMedicaConsultasComponent } from './modais/consultas/consultaMedica-modal-consultas.component';
import { ConsultaMedicaExamesComponent } from './modais/exames/consultaMedica-modal-exames.component';
import { ConsultaMedicaImunizacoesComponent } from './modais/imunizacoes/consultaMedica-modal-imunizacoes.component';
import { ConsultaMedicaService } from './services/consultaMedica.service';

@Component({
  selector: 'app-consulta-medica',
  templateUrl: './consultaMedica.component.html',
  styleUrls: ['./consultaMedica.component.css']
})
export class ConsultaMedicaComponent implements OnInit {

  private lastSearch: string;
  protected form: FormGroup;
  protected dataSource: Observable<PacienteModel[]>;
  protected behaviorSubject: BehaviorSubject<PacienteModel[]> = new BehaviorSubject<PacienteModel[]>(null);
  protected displayedColumns: string[] = ['prontuario','cns','nomeMae', 'actions'];

  constructor(protected consultaMedicaService: ConsultaMedicaService, protected fb: FormBuilder, protected customDialog: MatDialog) {
    this.form = this.fb.group({
      cpf: new FormControl()
    });
    this.dataSource = this.behaviorSubject.asObservable();
  }

  ngOnInit() {
    this.searchConsultaMedica();

    this.form.get('cpf').valueChanges.subscribe(cpf => {
      if (!isNullOrUndefined(cpf) && (cpf as string).length === 11 && cpf != this.lastSearch) {
        this.lastSearch = cpf;
        this.searchConsultaMedica(cpf);
      } else if (isNullOrUndefined(cpf) || (cpf as string).length === 0) {
        this.lastSearch = cpf;
        this.searchConsultaMedica(cpf);
      }
    });
  }

  private searchConsultaMedica(cpf?: string) {
    this.consultaMedicaService.searchConsultaMedica(cpf).subscribe((data: PacienteModel[]) => {
      if (!isNullOrUndefined(data)) {
        this.behaviorSubject.next(data);
      } else {
        this.behaviorSubject.next([]);
      }
    });
  }

  public abrirConsultas(paciente: PacienteModel) {
    const dialogRef = this.customDialog.open(
      ConsultaMedicaConsultasComponent
      , { data: paciente }
    );

    dialogRef.afterClosed().subscribe((result: PacienteModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.getValue().forEach(i => {
          if (i.cpf === result.cpf) {
            Object.assign(i, result);
          }
        });
      }
    });
  }


  public abrirExames(paciente: PacienteModel) {
    const dialogRef = this.customDialog.open(
      ConsultaMedicaExamesComponent
      , { data: paciente }
    );

    dialogRef.afterClosed().subscribe((result: PacienteModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.next(this.behaviorSubject.getValue().filter((paciente: PacienteModel) => {
          return paciente.cpf != result.cpf;
        }));
      }
    });
  }

  public abrirImunizacoes(paciente: PacienteModel) {
    const dialogRef = this.customDialog.open(
      ConsultaMedicaImunizacoesComponent
      , { data: paciente }
    );

    dialogRef.afterClosed().subscribe((result: PacienteModel) => {
      if (result != null && result != undefined) {
        this.behaviorSubject.next(this.behaviorSubject.getValue().filter((paciente: PacienteModel) => {
          return paciente.cpf != result.cpf;
        }));
      }
    });
  }

}
