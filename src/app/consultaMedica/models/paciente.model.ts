interface PacienteModel {
	id : number;
	cpf: string;
	cns: string;
	nome: string;
	nomeMae: string;
	sexo: string;
	raca: string;
	nacionalidade: string;
	municipioNascimento: string;
	ufNascimento: string;
	nascimento: string;
	email: string;
	telefone: string;
	endereco: string;
	cep: string;
	identidade: string;
	representanteLegal: string;
	historicoAlergico: string;
	diagnosticosAtivos: string;
	consultas: ConsultaModel[];
	imunizacoes: ImunizacaoModel[];
	exames: ExameProcedimentoModel[];

}