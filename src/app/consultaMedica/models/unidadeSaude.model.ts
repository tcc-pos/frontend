interface UnidadeSaudeModel {
    id: number;
    cnes: string;
    cnpj: string;
    tipoEstabelecimento: string;
    nomeFantasia: string;
    endereco: string;
    nome: string;
    cep: string;
    telefone: string;

}