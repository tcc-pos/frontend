interface ConsultaModel {
    id: number;
    data: string;
    tipo: string;
    especialidade: string;
    status: string;
    inicio: string;
    fim: string;
    motivoConsulta: string;
    historiaDoencaAtual: string;
    historicoFamiliar: string;
    exameFisico: string;
    hipoteseDiagnostico: string;
    diagnosticoConfirmado: boolean;
    terapia: string;
    codigoAtestado: string;
    codigoEncaminhamento: string;
    unidadeSaude: UnidadeSaudeModel;
    medico: MedicoModel;
}