interface ImunizacaoModel {
    id: number;
    dataAdministracao: string;
    dose: string;
    nomeImunobiologico: string;
}