interface ExameProcedimentoModel {
    id: number;
    grupo: string;
    subGrupo: string;
    nome: string;
    data: string;
}