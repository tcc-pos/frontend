import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaMedicaComponent } from '../consultaMedica.component';

const routes: Routes = [
    {
        path: '',
        component: ConsultaMedicaComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConsultaMedicaRoutingModule { }