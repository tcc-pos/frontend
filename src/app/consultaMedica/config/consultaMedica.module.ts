import { NgModule } from '@angular/core';

import { ConsultaMedicaRoutingModule } from './consultaMedica-routing.module';
import { ConsultaMedicaComponent } from '../consultaMedica.component';
import { MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatTableModule, MatTabsModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { CommonModule } from '@angular/common';
import { ConsultaMedicaConsultasComponent } from '../modais/consultas/consultaMedica-modal-consultas.component';
import { ConsultaMedicaImunizacoesComponent } from '../modais/imunizacoes/consultaMedica-modal-imunizacoes.component';
import { ConsultaMedicaExamesComponent } from '../modais/exames/consultaMedica-modal-exames.component';

@NgModule({
    declarations: [
        ConsultaMedicaComponent,
        ConsultaMedicaConsultasComponent,
        ConsultaMedicaImunizacoesComponent,
        ConsultaMedicaExamesComponent
    ],
    imports: [
        ConsultaMedicaRoutingModule,
        MatTableModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatTabsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        CommonModule,
        NgxMaskModule.forRoot()
    ],
    exports: [
        ConsultaMedicaComponent
    ],
    entryComponents: [
        ConsultaMedicaConsultasComponent,
        ConsultaMedicaImunizacoesComponent,
        ConsultaMedicaExamesComponent
    ]
})
export class ConsultaMedicaModule { }