import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-not-found',
    template: `
      <h2>
        Você não tem permissão para acessar essa página.
      </h2>
      <p>Talvez queira ir para a página de <a routerLink="">login</a></p>
    `
})
export class ForbiddenComponent implements OnInit {
    constructor() { }

    ngOnInit() {

    }
}