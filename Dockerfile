# Stage 1
FROM node:10-alpine as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build --prod

# Stage 2
FROM nginx:1.18.0-alpine
COPY .nginx/default.conf /etc/nginx/templates/default.conf.template
COPY docker-defaults.sh /
RUN chmod +x /docker-defaults.sh
COPY --from=build-step /app/dist/* /usr/share/nginx/html
ENTRYPOINT ["/docker-defaults.sh"]
CMD ["nginx", "-g", "daemon off;"]
